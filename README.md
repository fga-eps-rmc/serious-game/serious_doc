## Sobre

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam malesuada in ante quis porttitor. Donec eu cursus lectus. Ut bibendum dui eu metus interdum, vel pellentesque orci viverra. Fusce faucibus, justo id condimentum vulputate, ex nunc sagittis sem, ut facilisis tellus orci a nulla. Cras maximus accumsan placerat. Sed hendrerit orci vitae orci posuere consectetur. Aliquam vitae condimentum leo. Proin id gravida nunc, id placerat urna. Nam nec quam sagittis arcu laoreet accumsan.

Aplicação disponível em: 


## Contribuir
Para contribuir com esse projeto é importante seguir nosso [Guia de Contribuição](https://) do repositório e seguir nosso [Código de Conduta]().

## Ambientes

- [Documentação](https://gitlab.com/fga-eps-rmc/serious-game/serious_doc)

## Integrantes

### 2024-1

| Matricula | Nome | Gitlab | E-mail |
|-----------|------|--------|--------|
|  |  |  |  |
